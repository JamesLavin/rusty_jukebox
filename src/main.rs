extern crate rodio;
extern crate rusty_jukebox;
use rodio::Sink;
// use std::thread;

fn main() {
    rusty_jukebox::interaction::ask_name();
    println!("Rusty Jukebox: On");

    // println!("{}", rodio::default_output_device().unwrap());

    let _has_output_device = match rodio::default_output_device() {
        None => {
            println!("No audio device available for output");
            false
        }
        Some(device) => {
            // let input_formats = device.supported_input_formats().into_iter().map(|f| f.data_type);
            let input_formats = match device.supported_input_formats() {
                Ok(input_formats) => input_formats.count(),
                // Ok(input_formats) => input_formats.map(|f| f.data_type),
                Err(_err) => {
                    println!("No supported input formats");
                    panic!("No supported input formats")
                }
            };
            println!(
                "Output device available: {} (supporting: {})",
                device.name(),
                input_formats
            );
            true
        }
    };

    let device = rodio::default_output_device().unwrap();
    let sink = Sink::new(&device);

    // TODO: Create an assets directory
    let filenames = vec!["./sidney_bechet.ogg", "./bags_groove.mp3"];

    rusty_jukebox::playlist::load_songs(&filenames, &sink);

    sink.sleep_until_end();

    // thd.join();
    // std::thread::sleep(std::time::Duration::from_secs(100));

    // TODO: Spawn `rodio::play_once` in thread and join threads & remove hacky `sleep` code
    // music.join();
}

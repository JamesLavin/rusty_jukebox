pub mod interaction {
    use std::io;

    pub fn ask_name() {
        println!("What is your name?");
        let mut name = String::new();
        io::stdin()
            .read_line(&mut name)
            .expect("Failed to read user's name");
        println!("Hello, {}", name);
    }
}

pub mod playlist {
    use std::fs::File;
    use std::io::BufReader;
    // use rodio::Source;

    pub fn load_songs(filenames: &std::vec::Vec<&str>, sink: &rodio::Sink) {
        for fname in filenames {
            load_song(fname, sink);
        }
    }

    pub fn load_song(filename: &str, sink: &rodio::Sink) {
        println!("Loading {}", filename);
        let file = File::open(filename).unwrap();
        let source = rodio::Decoder::new(BufReader::new(file)).unwrap();
        sink.append(source);
    }
}
